const boxes = document.querySelectorAll('.container .box');

const colors = [
  '#037ef3','#00c16e','#0cb9c1','#f48924',
  '#f85a40','#ffc845','#52565e','#caccd1'
];

const noOfColors = colors.length;
const noOfBoxes = boxes.length;

var clickedElements = [];
var startTime;
var endTime;

var moves = 0;

document.getElementById('moves').innerText = moves;

function assignIndextoBox(box, i){
    box.id = i;
}

function assignColorToBox(box, i){
    if(i >= noOfColors){
        box.style.backgroundColor = colors[i-noOfColors];
    }else{
        box.style.backgroundColor = colors[i];
    }
}

function randmizeColor(box, i){
    box.style.order = parseInt(Math.random()*noOfBoxes);
}

function freezeBoxes(){
    var box1ID =  clickedElements[0].id;
    var box2ID =  clickedElements[1].id;

    var box1 = document.getElementById(box1ID);
    var box2 = document.getElementById(box2ID);

    box1.classList.add('freeze');
    box2.classList.add('freeze');

    clickedElements = [];
}

function hideThemAfterSomeTime(){
    setTimeout(function(){
        var box1ID =  clickedElements[0].id;
        var box2ID =  clickedElements[1].id;

        var box1 = document.getElementById(box1ID);
        var box2 = document.getElementById(box2ID);

        box1.classList.add('hideColor');
        box2.classList.add('hideColor');

        clickedElements = [];
    }, 200);
}

function checkIfGameFinished(){
    var freezedBoxes = document.querySelectorAll('.freeze');
    var finishedMsg = document.getElementById('finishedDiv');

    if(freezedBoxes.length == noOfBoxes){
        endTime();
        finishedMsg.classList.add('showFinished');
    }
}

function handleBoxClick(){
    if(this.classList.contains('freeze') == false){
        this.classList.remove('hideColor');

        var obj = {id:this.id, color:this.style.backgroundColor}
        clickedElements.push(obj);

        if(clickedElements.length == 2){
            if(clickedElements[0].color == clickedElements[1].color){
                freezeBoxes();
            }else{
                hideThemAfterSomeTime();
            }
            moves++;
            document.getElementById('moves').innerText = moves;
            console.log(moves);
        }
        checkIfGameFinished();
    }
}

function gameStart(){
    startTime();
    for(var i=0; i <= noOfBoxes; i++){
        assignIndextoBox(boxes[i], i);
        assignColorToBox(boxes[i], i);
        randmizeColor(boxes[i], i);

        boxes[i].addEventListener('click', handleBoxClick);
    }
}

function restart(){
    window.location.reload();
}

function startTime(){
    startTime = new Date();
}

function endTime(){
    endTime = new Date();
    var timeDiff = endTime - startTime;
    timeDiff /= 1000;
    var seconds = Math.round(timeDiff);
    var div = document.getElementById('timeFinished');
    document.getElementById('movesResult').innerText = moves;
    div.innerHTML += seconds+' seconds';
}

gameStart();
